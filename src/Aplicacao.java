import edu.cest.lab01.Pessoa;
import edu.cest.lab01.Cliente;
import edu.cest.lab01.Animal;
public class Aplicacao{
	public static void main(String[] args){
		Pessoa pess1 = new Pessoa();
		pess1.nome = "Arflizis";
		Cliente cli2 = new Cliente();
		cli2.nome = "Astolfo";
		Animal anim3 = new Animal();
		anim3.nome = "Dorft";
		System.out.println("Nome da pessoa: " + pess1.nome);
		System.out.println("Nome do Cliente: " + cli2.nome);
		System.out.println("Nome do Animal: " + anim3.nome);
		}
}
